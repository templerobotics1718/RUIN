#version 150

uniform mat4 projection;
uniform mat4 mvpMatrix;
uniform mat4 normalMatrix;
uniform sampler2D texture0;

in vec3 vPosition;
in vec3 vNormal;
in vec2 vTexCoord;

out vec3 fPosition;
out vec3 fNormal;
out vec2 fTexCoord;

void main() {
    fPosition = (mvpMatrix * vec4(vPosition, 1.0)).xyz;
    fNormal = normalize((normalMatrix * vec4(vNormal, 1.0)).xyz);
    fTexCoord = vTexCoord;

    gl_Position = vec4(fPosition, 1.0);
}
