#version 150

uniform sampler2D texture0;

in vec3 fPosition;
in vec3 fNormal;
in vec2 fTexCoord;

out vec3 color;

void main() {
    vec3 lightDirection = normalize(vec3(0.0, -0.5, 0.8));
    float diffuseIntensity = max(dot(lightDirection, fNormal), 0.0);
    float diffuseFactor = 0.5;
    float ambientFactor = 0.5;

    vec3 diffuse = texture(texture0, fTexCoord).xyz * diffuseIntensity * diffuseFactor;
    vec3 ambient = texture(texture0, fTexCoord).xyz * ambientFactor;
    vec3 specular = vec3(0.0, 0.0, 0.0);

    if (diffuseIntensity > 0.0) {
        vec3 eye = normalize(-fPosition);
        vec3 halfV = normalize(lightDirection + eye);
        
        specular = vec3(0.7, 0.7, 0.6) * pow(max(dot(halfV, fNormal), 0.0), 400);
    }

    color = ambient + diffuse + specular;
}
