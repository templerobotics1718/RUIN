cmake_minimum_required(VERSION 2.8.9)
project(RUIN CXX)

set(CMAKE_CXX_STANDARD 11)
set(CMAKE_CXX_STANDARD_REQUIRED ON)

set(CMAKE_MODULE_PATH ${CMAKE_MODULE_PATH}
                      cmake/modules)

find_package(Threads REQUIRED)
if (Threads_FOUND)
    link_libraries(${CMAKE_THREAD_LIBS_INIT})
endif()

find_package(OpenGL REQUIRED)
if (OpenGL_FOUND)
    include_directories(${OPENGL_INCLUDE_DIRS})
    link_libraries(${OPENGL_LIBRARIES})
endif()

find_package(GLEW REQUIRED)
if (GLEW_FOUND)
    include_directories(${GLEW_INCLUDE_DIRS})
    link_libraries(${GLEW_LIBRARIES})
endif()

find_path(GLM_PATH NAMES glm/glm.hpp)
if (GLM_PATH)
    include_directories(${GLM_PATH})
    if (NOT MSVC)
      add_definitions(-DGLM_FORCE_CXX11)
    endif()
else()
    message(SEND_ERROR "Cannot find GLM include directory")
endif()

find_path(ASIO_PATH NAMES asio.hpp)
if (ASIO_PATH)
    include_directories(${ASIO_PATH})
	add_definitions(-DASIO_STANDALONE)
else()
    message(SEND_ERROR "Cannot find Asio include directory")
endif()

find_package(SDL2 REQUIRED)
if (SDL2_FOUND)
    include_directories(${SDL2_INCLUDE_DIRS})
    string(STRIP "${SDL2_LIBRARIES}" SDL2_LIBRARIES)
    link_libraries(${SDL2_LIBRARIES})
    if (WIN32)
      # SDL2 will attempt to use SDL_main on Windows to
      # redefine program entry point
      # Following definition will prevent this
      add_definitions(-DSDL_MAIN_HANDLED)
    endif()
endif()

find_package(SDL2_image REQUIRED)
if (SDL2_image_FOUND)
    include_directories(${SDL2_IMAGE_INCLUDE_DIRS})
    link_libraries(${SDL2_IMAGE_LIBRARIES})
endif()

if (${CMAKE_CXX_COMPILER_ID} STREQUAL "GNU")
    link_libraries("atomic")
endif()

include_directories("lib/json/include")


file(GLOB_RECURSE SOURCES "src/*.cpp")
# ImGUI from source
file(GLOB IMGUI_SOURCES "lib/imgui/*.cpp")

file(GLOB_RECURSE RUIN_SOURCES "src/*.cpp")

include_directories("lib")
include_directories("src")

file(MAKE_DIRECTORY "bin")
set(CMAKE_RUNTIME_OUTPUT_DIRECTORY bin)

set(SOURCES ${IMGUI_SOURCES} ${RUIN_SOURCES})
add_executable(RUIN ${SOURCES})
