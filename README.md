# RUIN

RUIN is Temple Robotics' frontend mission control software.

It is designed for our 2018 NASA Robotic Mining Competition robot, to allow drivers to
robustly control the mining vehicle in a limited bandwith environment.

This is achieved by applying streaming pose and telemetry data to visualize a 3D model
of the robot in a virtual arena.

## Dependencies

RUIN currently uses the following libraries:
- OpenGL
- [GLEW](http://glew.sourceforge.net/)
- [GLM](https://glm.g-truc.net/0.9.8/index.html)
- [Asio](https://think-async.com/)
- [SDL 2.0](https://www.libsdl.org/index.php)
- [SDL_image 2.0](https://www.libsdl.org/projects/SDL_image/)

In addition, RUIN requires OpenGL 3.0 support or greater.

## Installation

On all operating systems, make sure to initialize and update the git submodules to get the latest version of GLM and ImGUI.

`git submodule init`
`git submodule update`

### Windows

#### Using Visual Studio

RUIN on Windows is built using CMake and the MSVC's NMAKE. To start, make sure you have CMake installed and added to your path directory, and Visual Studio 2017 Community Edition or newer.

Precompiled Windows libraries are available via TURMC's [Google Drive](https://drive.google.com/open?id=1gvRZhTwN4WOLZbFmiF81ZwBSTRYGtIJo). Extract the folder to any location.

Go to the Windows start menu and go under your `Visual Studio 2017` folder. Select 'x64 Native Tools Command Prompt for VS 2017' - this should launch a new terminal window. In this window, navigate to the `RUIN` directory. From here run `cmake . -DCMAKE_PREFIX_PATH=C:\Path\To\RUIN_Depend" -G "NMake Makefiles"`. If CMake has successfully complete, you can then run `nmake`. Once the build is completed, the RUIN executable should be in `RUIN/bin`.

### Ubuntu

RUIN uses the CMake build system with g++. If you don't have these installed, you can get them by running
`sudo apt-get install cmake g++`

To install RUIN's dependencies, run the following command: `sudo apt-get install mesa-common-dev libglew-dev libglm-dev libasio-dev libsdl2-dev libsdl2-image-dev`

Navigate to the RUIN directory, and run CMake using `cmake .`
Build the project by running `make`

If there are no errors, the executable should be generated as `bin/RUIN`

### Other Operating Systems

TODO
