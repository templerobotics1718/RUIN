#include<fstream>
#include<string>

#include "jsonconfig.hpp"

using std::string;
using std::fstream;
using nlohmann::json;

Config::Config(const std::string& filename)
{
	fstream file{ filename };

	if (file.is_open())
	{
		file >> json_file;

		file.close();
	}
}

//load_IP_add
string Config::load_IP_add()
{
	return json_file.at("IP_add");
}
//load_server_port
int Config::load_server_port() 
{
	return json_file.at("server_port");
}
//load_linear_vel
float Config::load_max_linear_vel() 
{
	return json_file.at("max_linear_vel");
}

//load_rotational_vel
float Config::load_max_rotational_vel() 
{
	return json_file.at("max_rotational_vel");
}
//load_scaling_factor
float Config::load_scaling_factor() 
{
	return json_file.at("scaling_factor");
}

/* int main() {
	string IP_address = "2220485543";
	Config c{ "C:\\C++Demo\\demo.json" };			//file_path 
	IP_address = c.load_IP_add();
	
	std::cout << IP_address << std::endl;
	system("pause");
	return 0;

} */