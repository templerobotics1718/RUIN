#pragma once

#include <string>

#include <nlohmann/json.hpp>

class Config
{
private:
	nlohmann::json json_file;

public:
	Config(const std::string& filename);

	std::string load_IP_add();
	int load_server_port();
	float load_max_linear_vel();
	float load_max_rotational_vel();
	float load_scaling_factor();
};
