#pragma once

#include <SDL2/SDL.h>

class Window {
    bool _initialized = false;
    SDL_Window* _window = nullptr;
    SDL_GLContext _context;

    public:
        Window() = default;

        ~Window() {
            destroy();
        }

        bool isInitialized() const {
            return _initialized;
        }

        bool intialize();
        void destroy();
        void swapBuffers() const;

        SDL_Window* getSdlWindow() const;
};
