#include "window.hpp"

#include <GL/glew.h>
#include <iostream>

bool Window::intialize() {
    SDL_GL_SetAttribute(SDL_GL_CONTEXT_PROFILE_MASK, SDL_GL_CONTEXT_PROFILE_CORE);
    SDL_GL_SetAttribute(SDL_GL_CONTEXT_MAJOR_VERSION, 3);
    SDL_GL_SetAttribute(SDL_GL_CONTEXT_MINOR_VERSION, 3);
    SDL_GL_SetAttribute(SDL_GL_DOUBLEBUFFER, 1);

    _window = SDL_CreateWindow("OpenGL window", SDL_WINDOWPOS_UNDEFINED,
                                SDL_WINDOWPOS_UNDEFINED, 1024, 768, SDL_WINDOW_OPENGL | SDL_WINDOW_RESIZABLE | SDL_WINDOW_INPUT_FOCUS | SDL_WINDOW_MAXIMIZED);

    if (!_window) {
        std::cout << "Failed to create SDL window: " << SDL_GetError() << "\n";
        return false;
    }

    _context = SDL_GL_CreateContext(_window);

    if (!_context) {
        std::cout << "Failed to create OpenGL 3.3 context: " << SDL_GetError() << "\n";
        return false;
    }

    glewExperimental = true;
    if (GLenum error = glewInit() != GLEW_OK) {
        std::cout << "GLEW intialization failed: " << glewGetErrorString(error) << "\n";
        return false;
    }

    int majorVersion, minorVersion;

    SDL_GL_GetAttribute(SDL_GL_CONTEXT_MAJOR_VERSION, &majorVersion);
    SDL_GL_GetAttribute(SDL_GL_CONTEXT_MAJOR_VERSION, &minorVersion);

    std::cout << "Running OpenGL version " << majorVersion << "." << minorVersion << "\n";

    glEnable(GL_DEPTH_TEST);

    _initialized = true;
    return true;
}

void Window::destroy() {
    if (_initialized) {
        SDL_GL_DeleteContext(_context);
        SDL_DestroyWindow(_window);
        _initialized = false;
    }
}

void Window::swapBuffers() const {
    if (_initialized) {
        SDL_GL_SwapWindow(_window);
    }
}

SDL_Window* Window::getSdlWindow() const {
    return _window;
}
