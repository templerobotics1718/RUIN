#include "util.hpp"

#include <iostream>

#include <GL/glew.h>

#ifdef __APPLE__
    #include <OpenGL/glu.h>
#else
    #include <GL/glu.h>
#endif

bool initVideo() {
    if (SDL_Init(SDL_INIT_VIDEO | SDL_INIT_JOYSTICK) != 0) {
        std::cout << "SDL initialization failed: " << SDL_GetError() << "\n";
        return false;
    }

    if (IMG_Init(IMG_INIT_PNG) != IMG_INIT_PNG) {
        std::cout << "SDL Image initialization failed: " << IMG_GetError() << "\n";
        return false;
    }

    return true;
}

void printOpenGlErrors() {
    GLenum error;

    while ((error = glGetError()) != GL_NO_ERROR) {
        std::cout << "OpenGL error: " << gluErrorString(error) << "\n";
    }
}

void clearBuffer(float r, float g, float b, float a) {
    glClearColor(0.2, 0.2, 0.2, 1.0);
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
}

void setViewport(unsigned int width, unsigned int height) {
    glViewport(0, 0, width, height);
}

void quitVideo() {
    IMG_Quit();
    SDL_Quit();
}
