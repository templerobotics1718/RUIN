#pragma once

#include <SDL2/SDL.h>
#include <SDL2/SDL_image.h>

bool initVideo();
void printOpenGlErrors();
void clearBuffer(float r, float g, float b, float a);
void setViewport(unsigned int width, unsigned int height);
void quitVideo();
