#include <iostream>
#include <thread>
#include <functional>
#include <atomic>

#include <imgui/imgui.h>
#include "gui/imgui_impl_sdl_gl3.h"

#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <glm/ext.hpp>

#include "video/video.hpp"
#include "rendering/rendering.hpp"

#include "threads/joystick.hpp"
#include "threads/telemetry.hpp"
#include "threads/remote_startup.hpp"

#include "json-config/jsonconfig.hpp"

#include "gui/network_launcher.hpp"
#include "gui/speedometer.hpp"
#include "gui/logger.hpp"
#include "gui/buttons.hpp"

GuiLogger logger;

int main(int argc, char* argv[]) {
    if (!initVideo())
        return 0;

    Window mainWindow;
    if (!mainWindow.intialize())
        return 0;

    Config config("../rc/config/config.json");
    float worldScale = config.load_scaling_factor();

    Model::Pointer world = Model::createEmpty(glm::scale(glm::mat4(1.0f),
                                              glm::vec3(worldScale, worldScale,
                                                        worldScale)));

    Mesh::Pointer testMesh = Mesh::fromObj("../rc/meshes/logo.obj");
    Texture::Pointer testTexture = Texture::fromFile("../rc/meshes/logo_diffuse.png");
    Shader::Pointer testShader = Shader::fromFile("../rc/shaders/simple/simple");

    Mesh::Pointer cubeMesh = Mesh::fromObj("../rc/meshes/cube.obj");
    Texture::Pointer cardboardTexture = Texture::fromFile("../rc/textures/cardboard.png");

    Mesh::Pointer wheelMesh = Mesh::fromObj("../rc/meshes/2018/wheel_2.obj");
    Texture::Pointer wheelTexture = Texture::fromFile("../rc/meshes/2018/wheel_2_albedo.png");

    Model::Pointer testModel = Model::createWithMesh(testMesh, testShader, testTexture);
    Model::Pointer testModel2 = Model::createWithMesh(wheelMesh, testShader, wheelTexture,
                                                      glm::scale(glm::translate(glm::mat4(1.0),
                                                                 glm::vec3(0.0f, 0.0f, 1.0f)),
                                                                 glm::vec3(0.4f, 0.4f, 0.4f)));

    world->addChild(testModel);
    //testModel->addChild(testModel2);

    RenderState state;

    glm::mat4 view = glm::eulerAngleYXZ(0.5f, 0.5f, 0.0f) *
                     glm::scale(glm::translate(glm::mat4(1.0), glm::vec3(0.0f, 0.0f, 0.0f)),
                                glm::vec3(0.6f, 0.6f, -0.6f));

    state.setViewMatrix(view);
    state.setProjectionMatrix(glm::perspective(60.0f, 4.0f/3.0f, 1.0f, 1000.0f));


    // Setup ImGui binding
    ImGui::CreateContext();
    ImGuiIO& io = ImGui::GetIO(); (void)io;
    //io.ConfigFlags |= ImGuiConfigFlags_NavEnableKeyboard;  // Enable Keyboard Controls
    
    // Setup style
    ImGui::StyleColorsDark();
    bool show_demo_window = true;
    bool show_another_window = false;
    ImVec4 clear_color = ImVec4(0.45f, 0.55f, 0.60f, 1.00f);


    SDL_Event event;

    bool running = true;
    float pitch = -0.5f;
    float yaw = 0.5f;
    unsigned char hat = 0;

    std::string ipAddress = config.load_IP_add();
    int serverPort = config.load_server_port();

    JoystickThread joyThread(config.load_max_linear_vel(),
                             config.load_max_rotational_vel());
    TelemetryThread telThread;
    RemoteStartupThread startThread(ipAddress, serverPort + 1);

    //joyThread.start(ipAddress, serverPort);
    //telThread.start(serverPort);

    NetworkLauncher netLauncher(std::bind(&RemoteStartupThread::start, &startThread),
                                [&]() { startThread.startAutonomous(); telThread.start(serverPort); },
                                std::bind(&RemoteStartupThread::shutdownAutonomous, &startThread),
                                [&]() { startThread.startManual();  
                                        joyThread.start(ipAddress, serverPort); },
                                [&]() { startThread.shutdownManual();
                                        joyThread.stop(); });

    Speedometer speedometer(std::bind(&JoystickThread::getLinearVelocity, &joyThread),
                            std::bind(&JoystickThread::getAngularVelocity, &joyThread));

    ButtonViewer buttons(joyThread);

    logger.log("Successfully started RUIN");

    while (running) {
        printOpenGlErrors();

        while (SDL_PollEvent(&event)) {
            switch (event.type) {
                case SDL_QUIT:
                    running = false;
                    break;
                case SDL_JOYHATMOTION:
                    hat = static_cast<unsigned int>(event.jhat.value);
                    break;
                case SDL_WINDOWEVENT:
                    switch (event.window.event) {
                        case SDL_WINDOWEVENT_SIZE_CHANGED:
                            {
                                float ratio = static_cast<float>(event.window.data1) /
                                              static_cast<float>(event.window.data2);
                                state.setProjectionMatrix(glm::scale(
                                        glm::perspective(60.0f, ratio, 0.0f, 1000.0f),
                                        glm::vec3(1.0f, 1.0f, 0.1f)));
                            
                                setViewport(event.window.data1, event.window.data2);
                            }
                            break;
                        default:
                            break;
                    }
                default:
                    joyThread.handleEvent(event);
                    break;
            }
            
            ImGui_ImplSdlGL3_ProcessEvent(&event);
        }

        ImGui_ImplSdlGL3_NewFrame(mainWindow.getSdlWindow());
        netLauncher.render();
        speedometer.render();
        logger.render();
        buttons.render();

        // Update joydata
        joyThread.updateFromMain();

        if (hat & SDL_HAT_LEFT) {
            yaw += 0.02f;
        } else if (hat & SDL_HAT_RIGHT) {
            yaw -= 0.02f;
        }

        if (hat & SDL_HAT_UP) {
            pitch += 0.02f;
        } else if (hat & SDL_HAT_DOWN) {
            pitch -= 0.02f;
        }

        view = glm::eulerAngleX(pitch) * glm::eulerAngleY(yaw) * 
                     glm::scale(glm::translate(glm::mat4(1.0), glm::vec3(0.0f, 0.0f, 0.0f)),
                                glm::vec3(0.6f, 0.6f, -0.6f));
        state.setViewMatrix(view);

        // testModel->origin = glm::rotate(glm::mat4(1.0f), rotation, glm::vec3(0.5f, 1.0f, 0.0f));
        testModel->origin = glm::translate(glm::mat4(1.0f), telThread.getPosition())  *
                            telThread.getRotation();

        clearBuffer(0.2f, 0.2f, 0.2f, 1.0f);
        world->render(state);
        
        ImGui::Render();
        ImGui_ImplSdlGL3_RenderDrawData(ImGui::GetDrawData());

        mainWindow.swapBuffers();
    }

    telThread.stop();
    startThread.stop();

    mainWindow.destroy();
    quitVideo();

    return 0;
}
