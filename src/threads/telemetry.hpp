#pragma once

#include <atomic>
#include <thread>

#include <asio.hpp>
#include <glm/glm.hpp>
#include <glm/gtc/quaternion.hpp>

using asio::ip::udp;

#pragma pack(1)
struct TelemetryPacket {
    struct Vec2 {
        float x, y;
    };

    struct Vec3 {
        float x, y, z;
    };

    struct Quaternion {
        float x, y, z, w;
    };

    Vec2 position;
    Quaternion orientation;
    Vec2 linearVelocity;
    Vec3 angularVelocity;
};

class TelemetryThread {
    private:
        std::atomic<TelemetryPacket> _packetAtomic;
        std::atomic<bool> _running;
        bool _threadStarted = false;

        std::thread _thread;

        asio::io_service _io_service;
        udp::socket _socket;
        unsigned int _port;

        void _updateThread();

    public:
        TelemetryThread();
        void start(unsigned int port);
        glm::vec3 getPosition() const;
        glm::mat4 getRotation() const;
        float getLinearVelocity() const;
        float getAngularVelocity() const;
        void stop();
};
