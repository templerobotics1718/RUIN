#include "joystick.hpp"

#include <iostream>
#include <chrono>
#include <cmath>

#include "../gui/logger.hpp"
extern GuiLogger logger;

const auto rate = std::chrono::milliseconds(500);
const auto secondDelay = std::chrono::seconds(1);
const int startupDelay = 10;

void JoystickThread::_updateThread() {
    logger.log("Starting joystick thread...");
    logger.log("Waiting " + std::to_string(startupDelay) +
               " seconds for hardware startup...");

    for (int i = 10; i > 0; i --) {
        logger.log("T-minus " + std::to_string(i) + " seconds...");
        std::this_thread::sleep_for(secondDelay);
    }
    logger.log("Attempting to connect to joystick server at " +
               _endpoint.address().to_string());

    asio::error_code error;
    _socket.connect(_endpoint, error);
    _io_service.run();

    if (error) {
        logger.log("Joystick failed to connect: " + error.message());
        _running = false;
        return;
    }

    logger.log("Successfully connected to joystick server");

    ControlPacket control[1];

    while (_running) {
        control[0] = _packetAtomic.load();

        _socket.send(asio::buffer(control));
        std::this_thread::sleep_for(rate);
    }

    logger.log("Disconnecting from joystick server");

    _socket.close();
    _io_service.run();
}

JoystickThread::JoystickThread(float maxVelocity, float maxRotation) :
    _running(false), _maxVelocity(maxVelocity), _maxRotation(maxRotation),
    _io_service(), _socket(_io_service)
{
    _ePressed = false;
};

void JoystickThread::start(const std::string& address, unsigned int port) {
    if (!_threadStarted) {
        _endpoint = udp::endpoint(asio::ip::address::from_string(address), port);
        _running = true;
        _thread = std::thread(&JoystickThread::_updateThread, this);;
        _threadStarted = true;
    }
}

void JoystickThread::updateFromMain() {
    ////////////////////////////////////////////////////////////////////////////////////////////
    // Change program here to populate the control packet with different values from the joystick
    ////////////////////////////////////////////////////////////////////////////////////////////
    
    ControlPacket packet;

    packet.x = 0;
    packet.y = 0;

    packet.aug_lift = 0;
    packet.aug_slide = 0;
    packet.aug_drive = 0;
    
    packet.belt_lift = 0;
    packet.belt_drive = 0;

    if (_ePressed) {
        packet.belt_drive = 2;
    }

    if (_joystick == nullptr) {
        _packetAtomic.store(packet);
        return;
    }

    float joyAxis_x = static_cast<float>(SDL_JoystickGetAxis(_joystick, 0))
                        / 32768.0f;

    float joyAxis_y = static_cast<float>(SDL_JoystickGetAxis(_joystick, 1))
                        / 32768.0f;

    if (SDL_JoystickGetButton(_joystick, 6)) {
        if (fabs(joyAxis_x) > 0.2f) {
            packet.x = -_maxRotation * joyAxis_x;
        }
        
        if (fabs(joyAxis_y) > 0.2f) {
            packet.y = -_maxVelocity * joyAxis_y;
        }
    }

    if (SDL_JoystickGetButton(_joystick, 4)) {
        packet.aug_lift = 1; // Raise
    }
    if (SDL_JoystickGetButton(_joystick, 2)) {
        packet.aug_lift = 2; // Lower
    }

    if (SDL_JoystickGetButton(_joystick, 5)) {
        packet.aug_slide = 1; // Raise
    }
    if (SDL_JoystickGetButton(_joystick, 3)) {
        packet.aug_slide = 2; // Lower
    }

    if (SDL_JoystickGetButton(_joystick, 0)) {
        if (SDL_JoystickGetButton(_joystick, 1)) {
            packet.aug_drive = 2; // Lower
        } else {
            packet.aug_drive = 1; // Raise
        }
    }

    if (SDL_JoystickGetButton(_joystick, 8)) {
        packet.belt_lift = 1; // Raise
    }
    if (SDL_JoystickGetButton(_joystick, 10)) {
        packet.belt_lift = 2; // Lower
    }

    if (SDL_JoystickGetButton(_joystick, 9)) {
        packet.belt_drive = 1;
    }

    _packetAtomic.store(packet);
}

void JoystickThread::handleEvent(const SDL_Event& event) {
    switch (event.type) {
        case SDL_JOYDEVICEADDED:
            logger.log("Opening new joystick device");

            if (_joystick != nullptr) {
                SDL_JoystickClose(_joystick);
                logger.log("Replacing existing joystick");
            }

            _joystick = SDL_JoystickOpen(event.jdevice.which);
            break;

        case SDL_JOYDEVICEREMOVED:
            logger.log("Removing joystick device");

            {
                ControlPacket packet = _packetAtomic.load();
                packet.x = 0;
                packet.y = 0;
                _packetAtomic.store(packet);
            }

            if (_joystick != nullptr) {
                SDL_JoystickClose(_joystick);
                _joystick = nullptr;
            }
            break;

        case SDL_KEYDOWN:
            if (event.key.keysym.sym == SDLK_e) {
                _ePressed = true;
            }
            break;

        case SDL_KEYUP:
            if (event.key.keysym.sym == SDLK_e) {
                _ePressed = false;
            }
            break;

        default:
            break;
    }
}

void JoystickThread::stop() {
    if (_threadStarted) {
        _running = false;
        _thread.join();
        _threadStarted = false;

        if (_joystick != nullptr) {
            SDL_JoystickClose(_joystick);
            _joystick = nullptr;
        }
    }
}

float JoystickThread::getLinearVelocity() {
    ControlPacket packet = _packetAtomic.load();

    return packet.y;
}

float JoystickThread::getAngularVelocity() {
    ControlPacket packet = _packetAtomic.load();

    return packet.x;
}

ControlPacket JoystickThread::getControlPacket() {
    return _packetAtomic.load();
}
