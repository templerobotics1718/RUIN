#include "remote_startup.hpp"

#include <iostream>
#include <chrono>

#include "../gui/logger.hpp"
extern GuiLogger logger;

const auto rate = std::chrono::seconds(10);

RemoteStartupThread::RemoteStartupThread(const std::string& address, unsigned int port) :
    _running(false), _io_service(), _socket(_io_service)
{
    _endpoint = tcp::endpoint(asio::ip::address::from_string(address), port);
}

void RemoteStartupThread::start() {
    if (!_threadStarted) {
        _running = true;
        _thread = std::thread(&RemoteStartupThread::_update, this);
        _threadStarted = true;
    }
}

void RemoteStartupThread::stop() {
    if (_threadStarted) {
        _running = false;
        _thread.join();
        _threadStarted = false;
    }
}

void RemoteStartupThread::startAutonomous() {
    std::lock_guard<std::mutex> lock(_queueMutex);
    _taskQueue.push(StartupTask::START_AUTONOMOUS);
}

void RemoteStartupThread::shutdownAutonomous() {
    std::lock_guard<std::mutex> lock(_queueMutex);
    _taskQueue.push(StartupTask::SHUTDOWN_AUTONOMOUS);
}

void RemoteStartupThread::startManual() {
    std::lock_guard<std::mutex> lock(_queueMutex);
    _taskQueue.push(StartupTask::START_MANUAL);
}

void RemoteStartupThread::shutdownManual() {
    std::lock_guard<std::mutex> lock(_queueMutex);
    _taskQueue.push(StartupTask::SHUTDOWN_MANUAL);
}

void RemoteStartupThread::_update() {
    logger.log("Starting remote startup thread...");
    logger.log("Attempting to connect to remote startup server at " + 
               _endpoint.address().to_string());

    asio::error_code error;
    _socket.connect(_endpoint, error);
    _io_service.run();

    if (error) {
        logger.log("Remote startup failed to connect: " + error.message());
        _running = false;
        return;
    }

    logger.log("Successfully connected to robot startup server");

    StartupPacket packet[1];
    StartupTask task = StartupTask::NONE;

    while (_running) {
        task = StartupTask::NONE;

        {
            std::lock_guard<std::mutex> lock(_queueMutex);

            if (!_taskQueue.empty()) {
                task = _taskQueue.front();
                _taskQueue.pop();
            }
        }

        if (task != StartupTask::NONE) {
            logger.log("Sending startup packet\n");
            packet[0] = _getPacketFromTask(task);
            _socket.send(asio::buffer(packet));
        }
    }

    _socket.close();
    _io_service.run();

    logger.log("Disconnecting from remote startup server");
}

StartupPacket RemoteStartupThread::_getPacketFromTask(const StartupTask task) {
    switch (task) {
        case StartupTask::START_AUTONOMOUS:
            return StartupPacket{"auto,1"};

        case StartupTask::SHUTDOWN_AUTONOMOUS:
            return StartupPacket{"auto,0"};

        case StartupTask::START_MANUAL:
            return StartupPacket{"nona,1"};

        case StartupTask::SHUTDOWN_MANUAL:
            return StartupPacket{"nona,0"};

        default:
            return StartupPacket{"err,9"};
    }
}
