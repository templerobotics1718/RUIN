#include "telemetry.hpp"

#include <iostream>

void TelemetryThread::_updateThread() {
    std::cout << "Starting telemetry thread...\n";
    std::cout << "Attempting to start telemetry server...\n";

    asio::error_code error;
    _socket.open(udp::v4());
    _socket.bind(udp::endpoint(udp::v4(), _port), error);
    _io_service.run();

    if (error) {
        std::cout << "Failed to start server: " << error.message() << "\n";
        _running = false;
        return;
    }

    std::cout << "Successfully started telemetry server\n";

    TelemetryPacket telemetry[1];

    while (_running) {
        _socket.async_receive(asio::buffer(telemetry), [](const asio::error_code&, size_t) {});
        _io_service.reset();
        _io_service.run();

        _packetAtomic.store(telemetry[0]);
    }
}

TelemetryThread::TelemetryThread() : _running(false), _io_service(), _socket(_io_service)
{
    TelemetryPacket startTelemetry{{0.0f, 0.0f}, {0.0f, 0.0f, 0.0f, 1.0f},
                              {0.0f, 0.0f}, {0.0f, 0.0f, 0.0f}};

    _packetAtomic.store(startTelemetry);
};

void TelemetryThread::start(unsigned int port) {
    if (!_threadStarted) {
        _port = port;
        _running = true;
        _thread = std::thread(&TelemetryThread::_updateThread, this);
        _threadStarted = true;
    }
}

glm::vec3 TelemetryThread::getPosition() const {
    TelemetryPacket tempPacket = _packetAtomic.load();

    return glm::vec3(tempPacket.position.x, 0.0f, tempPacket.position.y);
}

glm::mat4 TelemetryThread::getRotation() const {
    TelemetryPacket tempPacket = _packetAtomic.load();

    return glm::mat4(glm::quat(tempPacket.orientation.x, tempPacket.orientation.y,
                               tempPacket.orientation.z, tempPacket.orientation.w));
}

float TelemetryThread::getLinearVelocity() const {
    TelemetryPacket tempPacket = _packetAtomic.load();

    return tempPacket.linearVelocity.x;
}

float TelemetryThread::getAngularVelocity() const {
    TelemetryPacket tempPacket = _packetAtomic.load();

    return tempPacket.angularVelocity.z;
}

void TelemetryThread::stop() {
    if (_threadStarted) {
        _running = false;
        _socket.cancel();
        _thread.join();
        _threadStarted = false;
    }
}
