#pragma once

#include <atomic>
#include <thread>
#include <mutex>
#include <queue>

#include <asio.hpp>

using asio::ip::tcp;

#pragma pack(1)
struct StartupPacket {
    char message[7];
};

class RemoteStartupThread {
    public:
        RemoteStartupThread(const std::string& address, unsigned int port);
        void start();
        void stop();

        void startAutonomous();
        void shutdownAutonomous();
        void startManual();
        void shutdownManual();

    private:
        enum class StartupTask {
            START_AUTONOMOUS,
            SHUTDOWN_AUTONOMOUS,
            START_MANUAL,
            SHUTDOWN_MANUAL,
            NONE
        };

        std::queue<StartupTask> _taskQueue;
        std::mutex _queueMutex;

        std::thread _thread;
        std::atomic<bool> _running;
        bool _threadStarted = false;

        asio::io_service _io_service;
        tcp::endpoint _endpoint;
        tcp::socket _socket;

        void _update();

        static StartupPacket _getPacketFromTask(const StartupTask task);
};
