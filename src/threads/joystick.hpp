#pragma once

#include <atomic>
#include <thread>

#include <SDL2/SDL.h>
#include <asio.hpp>

using asio::ip::udp;

#pragma pack(1)
struct ControlPacket {
    ///////////////////////////////////////////////////////
    // Change packet structure here
    ///////////////////////////////////////////////////////
    
    float x, y;
    unsigned char aug_lift, aug_slide, aug_drive;
    unsigned char belt_lift, belt_drive;
    unsigned char user_data;
};

class JoystickThread {
    private:
        std::atomic<ControlPacket> _packetAtomic;
        std::atomic<bool> _running, _ePressed;
        float _maxVelocity, _maxRotation;
        bool _threadStarted = false;

        SDL_Joystick* _joystick = nullptr;

        std::thread _thread;

        asio::io_service _io_service;
        udp::socket _socket;
        udp::endpoint _endpoint;

        void _updateThread();

    public:
        JoystickThread(float maxVelocity, float maxRotation);
        void start(const std::string& address, unsigned int port);
        void updateFromMain();
        void handleEvent(const SDL_Event& event);
        void stop();

        float getLinearVelocity();
        float getAngularVelocity();
        ControlPacket getControlPacket();
};
