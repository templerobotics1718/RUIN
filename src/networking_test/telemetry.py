#!/usr/bin/env python

from socket import *
import struct

def main():
    client_socket = socket(AF_INET, SOCK_DGRAM)
    server_addr = ("127.0.0.1", 5005)

    message = struct.pack("=fffffffffff", 1, 2, 0.707, 0, 0, 0.707, 0, 0, 0, 0, 0)
    client_socket.sendto(message, server_addr)

if __name__ == "__main__":
    main()
