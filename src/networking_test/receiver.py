#!/usr/bin/env python

from socket import *

def main():
    server_socket = socket(AF_INET, SOCK_STREAM)
    server_addr = ("127.0.0.1", 5006)
    buffer_size = 12

    server_socket.bind(server_addr)
    server_socket.listen(1)
    conn, addr = server_socket.accept()

    try:
        while (1):
            data = conn.recv(buffer_size)
            print data
    except Exception as e:
        server_socket.close()

if __name__ == "__main__":
    main()