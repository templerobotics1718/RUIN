#include "simple_read.hpp"

std::string simpleFileRead(const std::string& filename) {
    std::stringstream stringStream;
    std::ifstream fileStream(filename, std::ios::in);
    std::string result = "";

    if (!fileStream.is_open()) {
        std::cout << "Failed to open file " << filename << "\n";
        return "";
    }

    stringStream << fileStream.rdbuf();
    result = stringStream.str();

    fileStream.close();
    return result;
}
