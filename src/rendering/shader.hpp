#pragma once

#include <string>
#include <memory>
#include <GL/glew.h>
#include <glm/glm.hpp>

#include "file/simple_read.hpp"

#define VERTEX_SUFFIX ".vert"
#define FRAGMENT_SUFFIX ".frag"

class RenderState;

class Shader {
    friend class RenderState;

    private:
        GLuint _program;
        GLuint _vertShader;
        GLuint _fragShader;
        bool _vertCompiled = false;
        bool _fragCompiled = false;
        bool _compiled = false;

        Shader();
        bool _setSource(const std::string& filename, GLuint shaderHandle);
        bool _compileShader(const GLuint shaderHandle);
        void _setAttribs();

    protected:
        void use();
        void setMatrix(const std::string& name, const glm::mat4& matrix);
        void bindTextures();

    public:
        using Pointer = std::shared_ptr<Shader>;

        bool setVertSource(const std::string& filename);
        bool setFragSource(const std::string& filename);
        bool compile();

        static Shader::Pointer create();
        static Shader::Pointer fromFile(const std::string& filename);
};
