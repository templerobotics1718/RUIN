#include "mesh.hpp"

#include <fstream>
#include <iostream>
#include <vector>
#include <regex>
#include <algorithm>

Mesh::Mesh() {
    glGenVertexArrays(1, &_array);
    glBindVertexArray(_array);

    glGenBuffers(2, _buffers);
    glBindBuffer(GL_ARRAY_BUFFER, _buffers[0]);
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, _buffers[1]);

    _setVertexAttribs();
}

Mesh::~Mesh() {
    glDeleteBuffers(2, _buffers);
    glDeleteVertexArrays(1, &_array);
}

void Mesh::_setVertexAttribs() {
    glEnableVertexAttribArray(0);
    glEnableVertexAttribArray(1);
    glEnableVertexAttribArray(2);
    glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, sizeof(Vertex), NULL);
    glVertexAttribPointer(1, 3, GL_FLOAT, GL_FALSE, sizeof(Vertex),
                          (void*)(sizeof(glm::vec3)));
    glVertexAttribPointer(2, 2, GL_FLOAT, GL_FALSE, sizeof(Vertex),
                          (void*)(sizeof(glm::vec3) * 2));
}

void Mesh::render() const {
    glBindVertexArray(_array);
    glDrawElements(GL_TRIANGLES, _numVerts, GL_UNSIGNED_INT, 0);
}

void Mesh::setVertexData(const std::vector<Vertex>& data) {
    glBindVertexArray(_array);
    glBufferData(GL_ARRAY_BUFFER, sizeof(Vertex) * data.size(),
                 data.data(), GL_STATIC_DRAW);
}

void Mesh::setIndexData(const std::vector<unsigned int>& data) {
    glBindVertexArray(_array);
    glBufferData(GL_ELEMENT_ARRAY_BUFFER, sizeof(unsigned int) * data.size(),
                 data.data(), GL_STATIC_DRAW);
    _numVerts = data.size();
}

Mesh::Pointer Mesh::create() {
    return Mesh::Pointer(new Mesh());
}

Mesh::Pointer Mesh::fromObj(const std::string& filename) {
    std::ifstream file(filename);

    if (!file.is_open()) {
        std::cout << "Cannot open OBJ file: " << filename << "\n";
        return nullptr;
    }

    std::vector<glm::vec3> positions, normals;
    std::vector<glm::vec2> texCoords;
    std::vector<Vertex> vertices;
    std::vector<unsigned int> indices;
    
    std::string line;
    std::regex attribute("^((?:v)|(?:vn)|(?:vt)) ([+-]?(?:\\d*\\.)?\\d+) "  
                          "([+-]?(?:\\d*\\.)?\\d+)(?:\\s?([+-]?(?:\\d*\\.)?\\d+))?\\s*$");
    std::regex face("^f (\\d+)(?:/(\\d*)/(\\d+))? (\\d+)(?:/(\\d*)/(\\d+))? "
                    "(\\d+)(?:/(\\d*)/(\\d+))?\\s*$");
    std::smatch match;

    while (std::getline(file, line)) {
        if (std::regex_match(line, match, attribute)) {
            if (match[1] == "v") {
                positions.emplace_back(std::stof(match[2]), std::stof(match[3]),
                                       std::stof(match[4]));
            } else if (match[1] == "vn") {
                normals.emplace_back(std::stof(match[2]), std::stof(match[3]),
                                     std::stof(match[4]));
            } else if (match[1] == "vt") {
                texCoords.emplace_back(std::stof(match[2]), 1 - std::stof(match[3]));
            }
        } else if (std::regex_match(line, match, face)) {
            for (unsigned char i = 0; i < 3; i++) {
                Vertex vertex;

                unsigned int positionIndex = std::stoi(match[i * 3 + 1]) - 1;
                if (positionIndex < positions.size()) {
                    vertex.position = positions[positionIndex];
                }

                if (match[i * 3 + 2].length() > 0) {
                    unsigned int texCoordIndex = std::stoi(match[i * 3 + 2]) - 1;
                    if (texCoordIndex < positions.size()) {
                        vertex.texCoord = texCoords[texCoordIndex];
                    }
                }

                if (match[i * 3 + 3].length() > 0) {
                    unsigned int normalIndex = std::stoi(match[i * 3 + 3]) - 1;
                    if (normalIndex < normals.size()) {
                        vertex.normal = normals[normalIndex];
                    }
                }

                auto result = std::find(vertices.begin(), vertices.end(), vertex);

                if (result == vertices.end()) {
                    vertices.push_back(vertex);
                    indices.push_back(vertices.size() - 1);
                } else {
                    indices.push_back((result - vertices.begin()));
                }
            }
        }
    }

    Mesh::Pointer result = Mesh::create();
    result->setVertexData(vertices);
    result->setIndexData(indices);

    file.close();
    return result;
}
