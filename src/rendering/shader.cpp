#include "shader.hpp"

Shader::Shader() {
    _program = glCreateProgram();

    _vertShader = glCreateShader(GL_VERTEX_SHADER);
    _fragShader = glCreateShader(GL_FRAGMENT_SHADER);
}

bool Shader::_setSource(const std::string& filename, GLuint shaderHandle) {
    std::string source = simpleFileRead(filename);
    char const* sourcePointer = source.c_str();

    glShaderSource(shaderHandle, 1, &sourcePointer, nullptr);

    return _compileShader(shaderHandle);
}

bool Shader::_compileShader(const GLuint shader) {
    if (!_compiled) {
        glCompileShader(shader);

        GLint param;
        glGetShaderiv(shader, GL_COMPILE_STATUS, &param);

        if (param != GL_TRUE) {
            std::cout << "Failed to compile shader:\n";

            glGetShaderiv(shader, GL_INFO_LOG_LENGTH, &param);

            char* compileLog = new char[param + 1];
            glGetShaderInfoLog(shader, param, nullptr, compileLog);

            std::cout << compileLog;

            delete[] compileLog;
            return false;
        }
    }

    return true;
}

void Shader::_setAttribs() {
    glBindAttribLocation(_program, 0, "vPosition");
    glBindAttribLocation(_program, 1, "vNormal");
    glBindAttribLocation(_program, 2, "vTexCoord");
}

bool Shader::setVertSource(const std::string& filename) {
    if (!_vertCompiled) {
        return _vertCompiled = _setSource(filename, _vertShader);
    }

    std::cout << "Cannot change vertex shader source: already successfully compiled\n";
    return false;
}

bool Shader::setFragSource(const std::string& filename) {
    if (!_fragCompiled) {
        return _fragCompiled = _setSource(filename, _fragShader);
    }

    std::cout << "Cannot change fragment shader source: already successfully compiled\n";
    return false;    
}

bool Shader::compile() {
    if (!_compiled) {
        if (!_vertCompiled || !_fragCompiled) {
            std::cout << "Cannot link shader program: " <<
                         "vertex or fragment shader not successfully compiled\n";
            return false;
        }

        glAttachShader(_program, _vertShader);
        glAttachShader(_program, _fragShader);

        _setAttribs();
        glLinkProgram(_program);

        GLint param;
        glGetProgramiv(_program, GL_LINK_STATUS, &param);

        if (param != GL_TRUE) {
            std::cout << "Shader program failed to link:\n";

            glGetProgramiv(_program, GL_INFO_LOG_LENGTH, &param);
            char* programLog = new char[param + 1];

            glGetProgramInfoLog(_program, param, nullptr, programLog);
            std::cout << programLog;

            delete[] programLog;

            return false;
        }

        glDetachShader(_program, _vertShader);
        glDetachShader(_program, _fragShader);

        glDeleteShader(_vertShader);
        glDeleteShader(_fragShader);

        _compiled = true;
        return true;
    }

    std::cout << "Shader program already linked and compiled: cannot rebuild\n";
    return false;
}

void Shader::use() {
    if (_compiled) {
        glUseProgram(_program);
    }
}

void Shader::setMatrix(const std::string& name, const glm::mat4& matrix) {
    glUniformMatrix4fv(glGetUniformLocation(_program, name.c_str()), 1, GL_FALSE,
                       &matrix[0][0]);
}

void Shader::bindTextures() {
    glUniform1i(glGetUniformLocation(_program, "texture0"), 0);
}

Shader::Pointer Shader::create() {
    return Shader::Pointer(new Shader());
}

Shader::Pointer Shader::fromFile(const std::string& filename) {
    Shader::Pointer shader(new Shader());

    shader->setVertSource(filename + VERTEX_SUFFIX);
    shader->setFragSource(filename + FRAGMENT_SUFFIX);
    shader->compile();

    return shader;
}
