#include "texture.hpp"

#include <iostream>

#include <SDL2/SDL.h>
#include <SDL2/SDL_image.h>

Texture::~Texture() {
    glDeleteTextures(1, &_texture);
}

void Texture::bind() const {
    glActiveTexture(GL_TEXTURE0);
    glBindTexture(GL_TEXTURE_2D, _texture);
}

Texture::Pointer Texture::fromFile(const std::string& filename) {
    SDL_Surface* surface = IMG_Load(filename.c_str());

    if (!surface) {
        std::cout << "Failed to load " << filename << ": " << IMG_GetError() << "\n";
        return nullptr;
    }

    GLuint texture;
    GLint format = (surface->format->BytesPerPixel == 4) ? GL_RGBA : GL_RGB;

    glGenTextures(1, &texture);
    glBindTexture(GL_TEXTURE_2D, texture);

    glTexImage2D(GL_TEXTURE_2D, 0, format, surface->w, surface->h, 0, format,
                 GL_UNSIGNED_BYTE, surface->pixels);

    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);

    glGenerateMipmap(GL_TEXTURE_2D);
    SDL_FreeSurface(surface);

    return Texture::Pointer(new Texture(texture));
}
