#pragma once

#include <vector>
#include <glm/glm.hpp>

#include "mesh.hpp"
#include "render_state.hpp"

// TODO: Define clear ownership semantics to avoid overuse of shared_ptr

class Model {
    private:
        Model* _parent = nullptr;
        std::vector<std::shared_ptr<Model> > _children;

        Model(glm::mat4 o = glm::mat4(1.0), Mesh::Pointer m = nullptr,
              Shader::Pointer s = nullptr, Texture::Pointer t = nullptr) :
            origin(o), mesh(m), shader(s), texture(t)
        {};

        void _removeChild(Model* child);

    public:
        using Pointer = std::shared_ptr<Model>;

        glm::mat4 origin;
        Mesh::Pointer mesh;
        Shader::Pointer shader;
        Texture::Pointer texture;

        ~Model();

        const void render(RenderState& state);
        const glm::mat4 getWorldTransform();

        void addChild(Model::Pointer child);
        void removeChild(Model::Pointer child);

        static Model::Pointer createWithMesh(Mesh::Pointer mesh,
                                             Shader::Pointer shader = nullptr,
                                             Texture::Pointer texture = nullptr,
                                             glm::mat4 origin = glm::mat4(1.0f));
        static Model::Pointer createEmpty(glm::mat4 origin = glm::mat4(1.0f));
};
