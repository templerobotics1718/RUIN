#pragma once

#include <vector>
#include <string>
#include <memory>

#include <GL/glew.h>
#include <glm/glm.hpp>

#include "render_state.hpp"

struct Vertex {
    glm::vec3 position;
    glm::vec3 normal;
    glm::vec2 texCoord;

    Vertex() = default;
    Vertex(glm::vec3 p, glm::vec3 n, glm::vec2 t) :
        position(p), normal(n), texCoord(t)
    {};
    bool operator==(const Vertex& vert) {
        return (position == vert.position) && (normal == vert.normal)
               && (texCoord == vert.texCoord);
    }
};

class Mesh {
    static_assert(sizeof(Vertex) == sizeof(float) * 8,
                  "Vertex structure is not packed correctly");

    private:
        GLuint _buffers[2];
        GLuint _array;
        GLuint _numVerts;

        Mesh();

        static void _setVertexAttribs();

    public:
        using Pointer = std::shared_ptr<Mesh>;

        ~Mesh();
    
        void render() const;
        void setVertexData(const std::vector<Vertex>& data);
        void setIndexData(const std::vector<unsigned int>& data);

        static Mesh::Pointer create();
        static Mesh::Pointer fromObj(const std::string& filename);
};
