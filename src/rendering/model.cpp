#include "model.hpp"

void Model::_removeChild(Model* child) {
    for (auto i = _children.begin(); i != _children.end(); i++) {
        if (i->get() == child) {
            (*i)->_parent = nullptr;
            _children.erase(i);
            break;
        }
    }
}

Model::~Model() {
    if (_parent != nullptr) {
        _parent->_removeChild(this);
    }

    for (auto i : _children) {
        i->_parent = nullptr;
    }
}

const void Model::render(RenderState& state) {
    state.pushModelStack();

    state.multModelMatrix(origin);

    if (mesh != nullptr) {
        state.setActiveShader(shader);
        state.setTexture(texture);
        state.update();
        mesh->render();
    }

    for (auto& x : _children) {
        x->render(state);
    }

    state.popModelStack();
}

const glm::mat4 Model::getWorldTransform() {
    if (_parent != nullptr) {
        return _parent->getWorldTransform() * origin;
    }

    return origin;
}

void Model::addChild(Model::Pointer child) {
    if (child->_parent == nullptr) {
        _children.push_back(child);
        child->_parent = this;
    }
}

void Model::removeChild(Model::Pointer child) {
    _removeChild(child.get());
}

Model::Pointer Model::createWithMesh(Mesh::Pointer mesh,
                                     Shader::Pointer shader,
                                     Texture::Pointer texture,
                                     glm::mat4 origin) {
    return Model::Pointer(new Model(origin, mesh, shader, texture));
}

Model::Pointer Model::createEmpty(glm::mat4 origin) {
    return Model::Pointer(new Model(origin));
}
