#pragma once

#include <memory>
#include <string>

#include <GL/glew.h>

class RenderState;
class Mesh;

class Texture {
    friend class RenderState;
    friend class Mesh;

    private:
        GLuint _texture;

        Texture(GLuint texture) :
            _texture(texture)
        {};

    protected:
        void bind() const;

    public:
        using Pointer = std::shared_ptr<Texture>;

        ~Texture();

        static Pointer fromFile(const std::string& filename);
};
