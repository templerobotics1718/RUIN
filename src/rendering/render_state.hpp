#pragma once

#include <memory>
#include <stack>
#include <glm/glm.hpp>

#include "shader.hpp"
#include "texture.hpp"

class RenderState {
    private:
        glm::mat4 _project;
        glm::mat4 _view;
        std::stack<glm::mat4> _modelStack;
        Shader::Pointer _activeShader;
        Texture::Pointer _texture;

        void _updateShaderParams();

    public:
        RenderState();

        void setActiveShader(Shader::Pointer shader);
        void update();

        void setTexture(Texture::Pointer texture);

        void setProjectionMatrix(const glm::mat4& projection);
        void setViewMatrix(const glm::mat4& view);
        void setModelMatrix(const glm::mat4& model);
        void multModelMatrix(const glm::mat4& mat);
        void pushModelStack();
        void popModelStack();

        glm::mat4 getProjectionMatrix() const;
        glm::mat4 getViewMatrix() const;
        glm::mat4 getModelMatrix() const;
};
