#include "render_state.hpp"

RenderState::RenderState() {
    _modelStack.push(glm::mat4(1.0f));
}

void RenderState::_updateShaderParams() {
    if (_activeShader != nullptr) {
        glm::mat4 modelView = _view * _modelStack.top();
        glm::mat4 normalMatrix = glm::transpose(glm::inverse(modelView));
        glm::mat4 mvpMatrix = _project * modelView;

        _activeShader->setMatrix("normalMatrix", normalMatrix);
        _activeShader->setMatrix("mvpMatrix", mvpMatrix);
    }
}

void RenderState::update() {
    _updateShaderParams();
    _activeShader->bindTextures();
}

void RenderState::setActiveShader(Shader::Pointer shader) {
    if (_activeShader != shader && shader != nullptr) {
        _activeShader = shader;
        _activeShader->use();
        _updateShaderParams();
    }
}

void RenderState::setTexture(Texture::Pointer texture) {
    if (_texture != texture && texture != nullptr) {
        _texture = texture;
        
        if (_texture != nullptr) {
            _texture->bind();
            _activeShader->bindTextures();
        }
    }
}

void RenderState::setProjectionMatrix(const glm::mat4& projection) {
    _project = projection;
}

void RenderState::setViewMatrix(const glm::mat4& view) {
    _view = view;
}

void RenderState::setModelMatrix(const glm::mat4& model) {
    _modelStack.top() = model;
}

void RenderState::multModelMatrix(const glm::mat4& mat) {
    _modelStack.top() *= mat;
}

void RenderState::pushModelStack() {
    _modelStack.push(_modelStack.top());
}

void RenderState::popModelStack() {
    _modelStack.pop();
}

glm::mat4 RenderState::getProjectionMatrix() const {
    return _project;
}

glm::mat4 RenderState::getViewMatrix() const {
    return _view;
}

glm::mat4 RenderState::getModelMatrix() const {
    return _modelStack.top();
}
