#pragma once

#include <functional>

#include <imgui/imgui.h>

class NetworkLauncher {
    public:
        using CallbackFunction = const std::function<void(void)>;

        NetworkLauncher(CallbackFunction launchFunction,
                        CallbackFunction autonomousStartup,
                        CallbackFunction autonomousShutdown,
                        CallbackFunction manualStartup,
                        CallbackFunction manualShutdown);

        void update();
        void render();

    private:
        enum class LauncherState {
            INITIAL,
            CONNECTED_NOT_RUNNING,
            RUNNING,
            STOPPED
        };

        enum class RunMode {
            AUTONOMOUS_MODE,
            MANUAL_MODE,
            ERROR
        };

        int _radioSelection = 0;

        LauncherState _state = LauncherState::INITIAL;
        RunMode _mode = RunMode::ERROR;

        CallbackFunction _launchFunction;
        CallbackFunction _autonomousStartup;
        CallbackFunction _autonomousShutdown;
        CallbackFunction _manualStartup;
        CallbackFunction _manualShutdown;

        static RunMode _getModeFromRadioSelection(int selection);
        static std::string _getModeString(RunMode mode);
};
