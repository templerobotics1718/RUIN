#include "logger.hpp"

GuiLogger::GuiLogger(unsigned int maxLines) :
    _maxLines(maxLines)
{}

void GuiLogger::render() {
    ImGui::Begin("Output");

    std::lock_guard<std::mutex> lock(_mutex);

    for (auto i = _queue.begin(); i != _queue.end(); i++) {
        ImGui::Text("%s", i->c_str());
    }
    ImGui::SetScrollHere(1.0f);
    ImGui::End();
}

void GuiLogger::log(const std::string& str) {
    std::lock_guard<std::mutex> lock(_mutex);

    _queue.push_back(str);
    if (_queue.size() > _maxLines) {
        _queue.erase(_queue.begin());
    }
}
