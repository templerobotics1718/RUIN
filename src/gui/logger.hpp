#pragma once

#include <string>
#include <mutex>
#include <vector>

#include <imgui/imgui.h>

class GuiLogger {
    public:
        GuiLogger(unsigned int maxLines = 80);

        void render();
        void log(const std::string& str);

    private:
        std::mutex _mutex;
        std::vector<std::string> _queue;
        unsigned int _maxLines;
};
