#pragma once

#include <string>

#include <imgui/imgui.h>

#include "../threads/joystick.hpp"

class ButtonViewer {
    public:
        ButtonViewer(JoystickThread& joyThread);

        void render();

    private:
        JoystickThread& _joyThread;

        static const char* _getDirectionString(const unsigned char direction);
};
