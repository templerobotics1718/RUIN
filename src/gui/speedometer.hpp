#pragma once

#include <functional>

#include <imgui/imgui.h>

class Speedometer {
    public:
        Speedometer(std::function<float(void)> getLinearVelocity,
                    std::function<float(void)> getAngularVelocity);

        void render();

    private:
        const std::function<float(void)> _getLinearVelocity;
        const std::function<float(void)> _getAngularVelocity;
};
