#include "network_launcher.hpp"

#include <iostream>

NetworkLauncher::NetworkLauncher(CallbackFunction launchFunction,
                                 CallbackFunction autonomousStartup,
                                 CallbackFunction autonomousShutdown,
                                 CallbackFunction manualStartup,
                                 CallbackFunction manualShutdown) :
    _launchFunction(launchFunction), _autonomousStartup(autonomousStartup),
    _autonomousShutdown(autonomousShutdown), _manualStartup(manualStartup),
    _manualShutdown(manualShutdown)
{}

void NetworkLauncher::update() {
    
}

void NetworkLauncher::render() {
    ImGui::Begin("Network Launcher");

    switch (_state) {
        case LauncherState::INITIAL:
            if (ImGui::Button("Connect to robot...")) {
                _launchFunction();
                _state = LauncherState::CONNECTED_NOT_RUNNING;
            }
            break;

        case LauncherState::CONNECTED_NOT_RUNNING:
            ImGui::RadioButton("Autonomous Mode", &_radioSelection, 0);
            ImGui::RadioButton("Manual Mode", &_radioSelection, 1);

            if (ImGui::Button("Start")) {
                _state = LauncherState::RUNNING;
                _mode = _getModeFromRadioSelection(_radioSelection);
                
                switch (_mode) {
                    case RunMode::AUTONOMOUS_MODE:
                        _autonomousStartup();
                        break;

                    case RunMode::MANUAL_MODE:
                        _manualStartup();
                        break;

                    default:
                        break;
                }
            }
            break;
        
        case LauncherState::RUNNING:
            ImGui::Text("Running robot in %s mode", _getModeString(_mode).c_str());

            if (ImGui::Button("Stop")) {
                _state = LauncherState::STOPPED;

                switch(_mode) {
                    case RunMode::AUTONOMOUS_MODE:
                        _autonomousShutdown();
                        break;

                    case RunMode::MANUAL_MODE:
                        _manualShutdown();
                        break;

                    default:
                        break;
                }
            }
            break;

        case LauncherState::STOPPED:
            ImGui::Text("Stopped");
            ImGui::Text(" ");
            ImGui::Text("Please restart to reconnect");
            break;

        default:
            break;
    }

    ImGui::End();
}

NetworkLauncher::RunMode NetworkLauncher::_getModeFromRadioSelection(int selection) {
    switch (selection) {
        case 0:
            return RunMode::AUTONOMOUS_MODE;

        case 1:
            return RunMode::MANUAL_MODE;

        default:
            return RunMode::ERROR;
    }
}

std::string NetworkLauncher::_getModeString(NetworkLauncher::RunMode mode) {
    switch (mode) {
        case RunMode::AUTONOMOUS_MODE:
            return "autonomous";

        case RunMode::MANUAL_MODE:
            return "manual";

        default:
            return "ERROR";
    }
}
