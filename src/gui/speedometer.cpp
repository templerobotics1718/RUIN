#include "speedometer.hpp"

Speedometer::Speedometer(std::function<float(void)> getLinearVelocity,
                         std::function<float(void)> getAngularVelocity) :
    _getLinearVelocity(getLinearVelocity), _getAngularVelocity(getAngularVelocity)
{}

void Speedometer::render() {
    ImGui::Begin("Speed");
    ImGui::Text("Linear Velocity: %.2f m/s", _getLinearVelocity());
    ImGui::Text("Angular Velocity: %.2f m/s", _getAngularVelocity());
    ImGui::End();
}
