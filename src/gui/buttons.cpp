#include "buttons.hpp"

ButtonViewer::ButtonViewer(JoystickThread& joyThread) :
    _joyThread(joyThread)
{}

void ButtonViewer::render() {
    ControlPacket packet = _joyThread.getControlPacket();

    ImGui::Begin("Button Inputs");

    ImGui::Text("Auger Lift: %s", _getDirectionString(packet.aug_lift));
    ImGui::Text("Auger Extend: %s", _getDirectionString(packet.aug_slide));
    ImGui::Text("Auger Spin: %s", _getDirectionString(packet.aug_drive));
    ImGui::Text("Belt Extend: %s", _getDirectionString(packet.belt_lift));
    ImGui::Text("Belt Spin: %s", _getDirectionString(packet.belt_drive));

    ImGui::End();
}

const char* ButtonViewer::_getDirectionString(const unsigned char direction) {
    switch (direction)
    {
        case 0:
            return "Stopped";

        case 1:
            return "Forward";

        case 2:
            return "Reverse";

        default:
            return "Error";
    }
}
